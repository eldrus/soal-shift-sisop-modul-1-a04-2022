#!/bin/bash
folderPath=forensic_log_website_daffainfo_log
file="log_website_daffainfo.log"

#1
mkdir -p $folderPath

#2
awk -F ":" 'END{ printf "Rata-rata serangan adalah sebanyak %.4f requests per jam", (NR-1)/$3 }' $file > $folderPath/ratarata.txt

#3
awk -F '"' '{print $2}' $file | sort | uniq -c | sort | awk 'END{printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n\n" ,$2,$1}'>$folderPath/result.txt

#4
echo -e "Ada $(grep -c 'curl' $file) requests yang menggunakan curl sebagai user-agent\n" >> $folderPath/result.txt

#5
awk -F':|"' '{if($6 == 2)print $2}' "log_website_daffainfo.log" | sort | uniq >> $folderPath/result.txt