#!/bin/bash

isLogin=false
username=""
password=""
countPasswordTry=0
files=users/user.txt

mkdir -p users
touch ./users/user.txt
touch log.txt

AddLog(){
    echo "$(date +"%m/%d/%Y %T") $1: $2" >> log.txt
}

until [ $isLogin == true ]
do
    until [ "$username" != "" ]
    do  
        echo "🍆Login page🍆"
        echo -n "Enter username >> "
        read username
        if [ ${#username} == 0 ]
        then
            echo "username cannot be empty"
        fi
        password=""
    done
    until [ "$password" != "" ]
    do
        echo -n "Enter password >> "
        read password
        if [ ${#password} == 0 ]
        then
            echo "password cannot be empty"
        fi

        if grep -qFw "$username $password" $files
        then
            printf "Berhasil login!"
            AddLog "LOGIN" "INFO User $username logged in"
            isLogin=true
        else
            AddLog "LOGIN" "ERROR Failed login attempt on user $username"
            printf "username/password salah!\n"
            username=""
        fi
    done
done

printf "\nsilahkan memilih antara (dl (1-99)/att) >> "
read pilihan
if [ `echo $pilihan | awk '{print $1}'` == "dl" ]; then
    dirname=$(date +"%Y-%m-%d")"_"$username
    if [[ ! -f "$dirname.zip" ]]; then
        mkdir -p $dirname
    else
        unzip -P "$password" "$dirname.zip"
    fi
    jumlah=$(ls $dirname | wc -l)
    query=$(echo $pilihan | awk '{print $2'})
    jumlahFoto=$(expr $jumlah + $query)
    for (( i=query; i<=jumlahFoto;i++ ))
    do
        if [[ $i -lt 10 ]]
        then
            file="$dirname/""PIC_0$i"
        else
            file="$dirname/""PIC_$i"
        fi
        wget -q -O "$file" https://loremflickr.com/320/240
        echo "$file telah terunduh"
    done
    zip --password "$password" -r "$dirname.zip" "$dirname"
    rm -rf "$dirname"
elif [ `echo $pilihan | awk '{print $1}'` == "att" ]; then
    jumlah=$(grep -w -c $username log.txt)
    printf "ada $jumlah percobaan login"
fi
