#!/bin/bash
clear
username=""
password=""
isEightLong=0
hasCapitalLowerLetter=0
isAlphanumeric=0
isDifferentFromUsername=0
isValid=0

CheckPassword() {
    isEightLong=0
    hasCapitalLowerLetter=0
    isAlphanumeric=0
    isDifferentFromUsername=0

    if [ ${#2} -lt 8 ];then
        echo "Password minimal 8 karakter"
        password=""
    else
        isEightLong=1
    fi
    
    if ! [[ "$2" =~ [[:upper:]] ]] || ! [[ "$2" =~ [[:lower:]] ]];then
        echo "Password harus memiliki 1 huruf kapital dan 1 huruf kecil"
        password=""
    else
        hasCapitalLowerLetter=1
    fi
    
    if ! [[ "$2" =~ ^[[:alnum:]]+$ ]];then
        echo "Password harus alphanumeric"
        password=""
    else
        isAlphanumeric=1
    fi

    if [ $2 != $1 ];then
        isDifferentFromUsername=1
    else 
        echo "Password tidak boleh sama dengan username"
        password=""
    fi

    if [[ $isEightLong == 1 && $hasCapitalLowerLetter == 1 &&  $isAlphanumeric == 1 && $isDifferentFromUsername == 1 ]]
    then
        isValid=1
    fi
}
AddLog(){
    echo "$(date +"%m/%d/%Y %T") $1: $2" >> log.txt
}

mkdir -p users
touch ./users/user.txt
touch log.txt

until [ $isValid != 0 ]; do
    until [ "$username" != "" ]
    do
        echo "💦Register💦"
        echo -n "Enter username >> "
        read username
        if [ ${#username} == 0 ]
        then
            echo "username cannot be empty"
        fi
        checkIfExist=$(awk '$0 ~ username {if ($1 == username) print 1}' username=$username ./users/user.txt)
        if [[ $checkIfExist ]];then 
            clear
            echo "Username telah digunakan"
            username=""
            AddLog "REGISTER" "ERROR User already exists"
            continue
        fi
    done

    until [ "$password" != "" ]
    do
        echo -n "Enter password >> "
        read -s password
        if [ ${#password} == 0 ]
        then
            echo "password cannot be empty"
        fi
        CheckPassword "$username" "$password"
    done
done

echo "$username $password" >> ./users/user.txt
AddLog "REGISTER" "INFO User $username registered succesfully"
echo -e "\nusername $username sukses terdaftar!"