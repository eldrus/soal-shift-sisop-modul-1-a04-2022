#!/bin/bash
date=$(date '+%Y%m%d%H')
dir="$HOME/log/"
target="$(find /home/"$USER"/log/*metrics_"$(date '+%Y%m%d%H')"*)"
log="$dir""metrics_agg_""$date"".log"
sudo mkdir -p $dir
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" | sudo tee -a "$log"
printf "minimum," | sudo tee -a "$log"  && { readarray -t fs <<< $target; for i in "${fs[@]}"; do sed -n "2p" "$i"; done; } | cut -d"," -f1-10 | sort -n | head -1 | sudo tee -a "$log"
printf "maksimum," | sudo tee -a "$log"  && { readarray -t fs <<< $target; for i in "${fs[@]}"; do sed -n "2p" "$i"; done; } | cut -d"," -f1-10 | sort -n | tail -1 | sudo tee -a "$log" 
{ readarray -t fs <<< $target ;for i in "${fs[@]}" ; do sed -n 2p "$i"; done; } | awk -F'[,]' '{ for (i=1;i<=NF;i++){if(i==9) {sum[i]=$i} else {sum[i]+=$i}} } END { printf "average,"; for (i=1;i<=NF;i++){if(i==9) {printf sum[i]} else {printf sum[i]/NR}; if (i!=NF) {printf ","} else {printf "\n"}}}' | sudo tee -a "$log" 
