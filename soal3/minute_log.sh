#!/bin/bash
dir="$HOME/log/"
log=$dir/$(date +"metrics_%Y%m%d%H%M%S").log

sudo mkdir -p $dir
chmod 700 $dir

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $log
free -m $HOME | awk 'BEGIN{ORS=","} NR>1{for(i=2;i<=NF;i++)print $i}' >> $log
printf $HOME, >> $log
du -sh $HOME | awk '{print $1}' >> $log

chmod 400 $log