# laporan soal-shift-sisop-modul-1-A04-2022

## Anggota Kelompok
- Rafiqi Rachmat 5025201067
- sayid ziyad ibrahim Alaydrus 5025201147
- David Fischer Simajuntak 5025201123

## Soal No.1 
## A.
Han ingin membuat sistem register pada script register.sh dan setiap user yang berhasil
didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login
yang dibuat di script main.sh. Setelah login, user dapat mengetikkan 2 command dengan yaitu : "dl N" untuk mendownload gambar dan 'N' sebagai jumlah gambar, lalu "att" untuk menghitung percobaan login. 

```
mkdir -p users
touch ./users/user.txt
touch log.txt

AddLog(){
    echo "$(date +"%m/%d/%Y %T") $1: $2" >> log.txt
}

until [ $isLogin == true ]
do
    until [ "$username" != "" ]
    do  
        echo "🍆Login page🍆"
        echo -n "Enter username >> "
        read username
```
untuk membuat sistem register :
1. membuat file `user.txt` untuk menyimpan database username dan password yang telah terdaftar
2. pada sistem login, user akan diminta memasukkan username dan password sampai  registrasi berhasil

## B.
untuk menjaga keamanan input pada password harus tertutup/tidak terlihat yaitu menggunakan read -s 'password'
```    do
        echo -n "Enter password >> "
        read -s password
``` 
lalu ada beberapa ketentuan untuk memasukkan password :
``` 
CheckPassword() {
    isEightLong=0
    hasCapitalLowerLetter=0
    isAlphanumeric=0
    isDifferentFromUsername=0

    if [ ${#2} -lt 8 ];then
        echo "Password minimal 8 karakter"
        password=""
    else
        isEightLong=1
    fi
    
    if ! [[ "$2" =~ [[:upper:]] ]] || ! [[ "$2" =~ [[:lower:]] ]];then
        echo "Password harus memiliki 1 huruf kapital dan 1 huruf kecil"
        password=""
    else
        hasCapitalLowerLetter=1
    fi
    
    if ! [[ "$2" =~ ^[[:alnum:]]+$ ]];then
        echo "Password harus alphanumeric"
        password=""
    else
        isAlphanumeric=1
    fi

    if [ $2 != $1 ];then
        isDifferentFromUsername=1
    else 
        echo "Password tidak boleh sama dengan username"
        password=""
    fi
``` 
![ketentuan password](https://www.linkpicture.com/q/messageImage_1646568234342.jpeg)
1. password tidak boleh kurang dari 8 karakter
2. password harus memiliki min 1 huruf kapital dan huruf kecil
3. password harus alphanumeric
4. password tidak boleh sama dengan username
5. input password akan terus diulangi sampai password sesuai dengan ketentuan diatas.

## C. 
setiap percobaan login maupun register akan tercatat di `log.txt` dengan format ` MM/DD/YY hh:mm:ss`  ` LOGIN: INFO User admin logged in`. 
1. ketika register dengan username yang sudah terdaftar
``` checkIfExist=$(awk '$0 ~ username {if ($1 == username) print 1}' username=$username ./users/user.txt)
        if [[ $checkIfExist ]];then 
            clear
            echo "Username telah digunakan"
            username=""
            AddLog "REGISTER" "ERROR User already exists"
```  
2. ketika register berhasil, maka message akan muncul di log.txt.
``` echo "$username $password" >> ./users/user.txt 
AddLog "REGISTER" "INFO User $username registered succesfully"
echo -e "\nusername $username sukses terdaftar!"
``` 
3. ketika login dengan password yang salah 
``` 
AddLog "LOGIN" "ERROR Failed login attempt on user $username"
            printf "username/password salah!\n"
            username=""
``` 

4. login. ketika username dan password sama dengan di `user.txt` maka :
``` 
if grep -qFw "$username $password" $files
        then
            printf "Berhasil login!"
            AddLog "LOGIN" "INFO User $username logged in"
            isLogin=true
``` 


## D. 
setelah login user dapat mengetikkan 2 command 'dl N' dan 'att'
```
if [[ ! -f "$dirname.zip" ]]; then
        mkdir -p $dirname
    else
        unzip -P "$password" "$dirname.zip"
```
untuk fungsi 'dl N' sebelum mendownload gambar dilakukan pengecekan dahulu apakah sudah ada folder dari user tsb
1. jika folder belum ada maka akan membuat folder sesuai username dahulu `mkdir -p $dirname`
2. jika sudah ada , folder tsb akan diunzip terlebih dahulu `unzip -P "$password" "$dirname.zip"`

```
for (( i=query; i<=jumlahFoto;i++ ))
    do
        if [[ $i -lt 10 ]]
        then
            file="$dirname/""PIC_0$i"
        else
            file="$dirname/""PIC_$i"
```
foto-foto yang didownload akan memiliki format nama sesuai urutan download nya `$dirname/""PIC_$i` eg `PIC_01,PIC_02`
` zip --password "$password" -r "$dirname.zip" "$dirname"`
setelah selesai folder akan di-zip dan dipassword sesuai nama user.
![image downloaded](https://www.linkpicture.com/q/messageImage_1646568379743.jpeg)

``` 
`echo $pilihan | awk '{print $1}'` == "att" ]; then
    jumlah=$(grep -w -c $username log.txt)
    printf "ada $jumlah percobaan login"
```
perintah 'att' untuk mengetahui jumlah percobaan login. menggunakan fungsi `grep` untuk mengakses folder log.txt dan menghitung jumlah percobaan login dari tiap baris yang ada dengan awk.
![login attempt](https://www.linkpicture.com/q/messageImage_1646568422247.jpeg)

# Soal no. 2
website https://daffa.info di hack oleh seseorang yang tidak
bertanggung jawab. Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk
bernama "soal2_forensic_dapos.sh" 

``` #!/bin/bash
folderPath=forensic_log_website_daffainfo_log
file="log_website_daffainfo.log"

#1
mkdir -p $folderPath
``` 
## B. 
Dapos ingin mengetahui rata-rata request per jam yang dikirim. Jumlah rata-rata akan dimasukkan ke dalam file ratarata.txt di dalam folder sebelumnya.
`awk -F ":" 'END{ printf "Rata-rata serangan adalah sebanyak %.4f requests per jam", (NR-1)/$3 }' $file > $folderPath/ratarata.txt` 

## C. 
Tampilkan IP paling banyak melakukan request dan berapa banyak request yang dikirimkan. Masukkan hasil ke file baru bernama result.txt pada folder forensic_log_website_daffainfo_log
` awk -F '"' '{print $2}' $file | sort | uniq -c | sort | awk 'END{printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n\n" ,$2,$1}'>$folderPath/result.txt
` 
## D.
Berapa banyak request menggunakan user-agent curl. Masukkan hasil nya ke file result.txt yang sudah dibuat sebelumnya.
` echo -e "Ada $(grep -c 'curl' $file) requests yang menggunakan curl sebagai user-agent\n" >> $folderPath/result.txt`
[request](https://www.linkpicture.com/q/messageImage_1646568457520.jpeg)
## E. 
Daftar IP yang melakukan penyerangan pada jam 2 pagi di tanggal 23. Masukkan ke dalam file result.txt yang telah dibuat sebelumnya.
`awk -F':|"' '{if($6 == 2)print $2}' "log_website_daffainfo.log" | sort | uniq >> $folderPath/result.txt`

# Soal no.3
ubay meminta untuk membuat suatu program monitoring resources yang ada pada komputer. Buatlah program monitoring resource pada komputer kalian, cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path
yang akan dimonitor adalah /home/{user}/.

## A & B
``` 
dir="$HOME/log/"
log=$dir/$(date +"metrics_%Y%m%d%H%M%S").log

sudo mkdir -p $dir
chmod 700 $dir

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $log
free -m $HOME | awk 'BEGIN{ORS=","} NR>1{for(i=2;i<=NF;i++)print $i}' >> $log
printf $HOME, >> $log
du -sh $HOME | awk '{print $1}' >> $log
```
program untuk memonitoring ram dan size directory user. adapun langkah yang dapat dilakukan yaitu :
1. membuat file.log untuk men-store data ram dan size directory dengan format `log=$dir/$(date +"metrics_%Y%m%d%H%M%S").log`. 
2. lalu memonitor ram menggunakan `free -m` dan mengurutkan data menggunakan fungsi awk , untuk mengakses size directory menggunakan `du -sh`.
3. keluaran dari data tersebut akan disimpan pada folder yang telah ditentukan. 
lalu fungsi tersebut diharapkan dapat berjalan secara otomatis pada setiap menit  menggunakan ` crontab -e`    `* * * * * bash minute_log.sh`

![output monitoring](https://www.linkpicture.com/q/Screenshot-2022-03-06-172118.png)

## C.
Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis.
```
#!/bin/bash
date=$(date '+%Y%m%d%H')
dir="$HOME/log/"
target="$(find /home/"$USER"/log/*metrics_"$(date '+%Y%m%d%H')"*)"
log="$dir""metrics_agg_""$date"".log"
sudo mkdir -p $dir
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" | sudo tee -a "$log"
printf "minimum," | sudo tee -a "$log"  && { readarray -t fs <<< $target; for i in "${fs[@]}"; do sed -n "2p" "$i"; done; } | cut -d"," -f1-10 | sort -n | head -1 | sudo tee -a "$log"
printf "maksimum," | sudo tee -a "$log"  && { readarray -t fs <<< $target; for i in "${fs[@]}"; do sed -n "2p" "$i"; done; } | cut -d"," -f1-10 | sort -n | tail -1 | sudo tee -a "$log" 
{ readarray -t fs <<< $target ;for i in "${fs[@]}" ; do sed -n 2p "$i"; done; } | awk -F'[,]' '{ for (i=1;i<=NF;i++){if(i==9) {sum[i]=$i} else {sum[i]+=$i}} } END { printf "average,"; for (i=1;i<=NF;i++){if(i==9) {printf sum[i]} else {printf sum[i]/NR}; if (i!=NF) {printf ","} else {printf "\n"}}}' | sudo tee -a "$log" 
```
pada soal no.3c script digunakan untuk mencari nilai minimum maximum dan rata rata dari file yang tergenerate setiap menitnya. dalam proses soal ini menggunakan awk dan dengan mengguanakan echo untuk proses input. Dengan target file ` target="$(find /home/"$USER"/log/*metrics_"$(date '+%Y%m%d%H')"*)"` 
setelah prosses looping selesai kemudian data d sort , output akan dimasukkan ke file log yang telah ditentukan dengan format `log="$dir""metrics_agg_""$date"".log"` dengan sudo tee.
![min,max,avg](https://www.linkpicture.com/q/Screenshot-2022-03-06-172305.png)







